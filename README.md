# Website files of FSCI 
[![pipeline status](https://gitlab.com/fsci/fsci.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/fsci/fsci.gitlab.io/-/commits/master)
### Build Requirements:

- Hugo
- Git

### USAGE:

It's good to have basic knowledge of Hugo, git & markdown. Don't worry, It's easy to learn them :-)

This [hugo tutorial playlist](https://invidio.us/playlist?list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3) will help you to understand hugo easily.

##### **To create a new statement:**

- Clone this repo & `cd` into it

- Run `hugo new blog/title-of-the-new-statement.md`

- A new markdown file will be created under `content/statements/` folder, Use your favourite text editor to edit it. 

- To see website preview, run `hugo server -D`

- Open `localhost:1313` in your browser

- When you finish drafting the statement, To make it live, set `draft: true` & add relevant `tags` in your file.

- Commit your changes & git push them, That's it :-) 
